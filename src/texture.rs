use std::f32::consts::TAU;
use std::ops::{Index, IndexMut};
use std::path::Path;

use image::{GenericImageView, ImageBuffer, ImageResult, Pixel, Rgb};
use imageproc::point::Point;
use imageproc::rect::Rect;
use palette::color_difference::Wcag21RelativeContrast;
use palette::Srgb;
use rustfft::num_traits::ToPrimitive;
use toodee::{TooDee, TooDeeOps};

use crate::num_complex::Complex;

#[derive(Copy, Clone, Debug)]
pub struct TextureProperties {
    pub exposure: f32,
    pub gamma: f32,
    pub coherent_light: bool,
}

impl Default for TextureProperties {
    fn default() -> Self {
        Self {
            exposure: 0.0,
            gamma: 1.0,
            coherent_light: false,
        }
    }
}

/// A texture which consists of floating point complex numbers.
// TODO: Use <T> instead of f32?
#[derive(Clone, Debug)]
pub struct Texture<T: Default> {
    // TODO: Only use toodee internally and expose a reference to the Vec to the user?
    pub pixels: TooDee<T>,
}

impl<T: Default> Texture<T> {
    pub fn new(width: usize, height: usize) -> Self {
        Self {
            pixels: TooDee::new(width, height),
        }
    }

    /// Return the texture's width.
    pub fn width(&self) -> usize {
        self.pixels.num_cols()
    }

    /// Return the texture's height.
    pub fn height(&self) -> usize {
        self.pixels.num_rows()
    }

    /// Return the texture's dimensions.
    pub fn dimensions(&self) -> (usize, usize) {
        (self.width(), self.height())
    }
}

impl Texture<Complex<f32>> {
    /// Create a new texture from an existing file.
    pub fn from_file<P: AsRef<Path>>(path: P) -> ImageResult<Self> {
        Ok(Self::from_generic_image(&image::open(path)?))
    }

    /// Create a new texture from a struct
    /// implementing the `GenericImageView` trait.
    pub fn from_generic_image<I: GenericImageView>(image: &I) -> Self {
        let width = image.width() as usize;
        let height = image.height() as usize;

        let mut pixels = TooDee::new(width, height);

        for (x, y, pixel) in image.pixels() {
            pixels[y as usize][x as usize] =
                Complex::new(pixel.to_rgb()[0].to_f32().unwrap() / 255.0, 0.0);
        }

        Self { pixels }
    }

    /// Create a new polygon texture with a specified amount of vertices.
    ///
    /// Note that the size is relative to the image dimensions,
    /// meaning that 1.0 fills the whole image.
    pub fn polygon(width: u32, height: u32, vertices: usize, size: f32) -> Self {
        let mut buffer = ImageBuffer::new(width, height);

        let half_width = width / 2;
        let half_height = height / 2;

        let mut points = vec![Point::new(0, 0); vertices];
        points.iter_mut().enumerate().for_each(|(i, x)| {
            let t = TAU * i as f32 / vertices as f32;
            *x = Point::new(
                (half_width as f32 + (t.sin() * size * half_width as f32)) as i32,
                (half_height as f32 + (t.cos() * size * half_height as f32)) as i32,
            );
        });

        imageproc::drawing::draw_polygon_mut(&mut buffer, &points, Rgb([255_u8, 255_u8, 255_u8]));

        Self::from_generic_image(&buffer)
    }

    /// Create a new single slit texture.
    pub fn single_slit(width: u32, height: u32, slit_width: u32, slit_height: u32) -> Self {
        let mut buffer = ImageBuffer::new(width, height);

        let rect = Rect::at(
            (width / 2 - slit_width / 2) as i32,
            (height / 2 - slit_height / 2) as i32,
        )
        .of_size(slit_width, slit_height);

        imageproc::drawing::draw_filled_rect_mut(&mut buffer, rect, Rgb([255_u8, 255_u8, 255_u8]));

        Self::from_generic_image(&buffer)
    }

    /// Create a new double slit texture.
    pub fn double_slit(
        width: u32,
        height: u32,
        slit_width: u32,
        slit_height: u32,
        // TODO: Make this the distance between the centers, not the ends
        distance: u32,
    ) -> Self {
        let mut buffer = ImageBuffer::new(width, height);

        let rect1 = Rect::at(
            (width / 2 - slit_width - distance / 2) as i32,
            (height / 2 - slit_height / 2) as i32,
        )
        .of_size(slit_width, slit_height);
        let rect2 = Rect::at(
            (width / 2 + distance / 2) as i32,
            (height / 2 - slit_height / 2) as i32,
        )
        .of_size(slit_width, slit_height);

        imageproc::drawing::draw_filled_rect_mut(&mut buffer, rect1, Rgb([255_u8, 255_u8, 255_u8]));
        imageproc::drawing::draw_filled_rect_mut(&mut buffer, rect2, Rgb([255_u8, 255_u8, 255_u8]));

        Self::from_generic_image(&buffer)
    }

    /// Create a new texture from a closure which takes the x and y coordinates as input
    /// and returns a floating point value.
    pub fn from_closure<F: Fn(usize, usize) -> f32>(width: usize, height: usize, map: F) -> Self {
        let mut pixels = TooDee::new(width, height);

        for y in 0..width {
            for x in 0..height {
                pixels[y][x] = Complex::new(map(x, y), 0.0);
            }
        }

        Self { pixels }
    }

    pub fn to_image_buffer(
        &self,
        texture_properties: TextureProperties,
    ) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
        let pixels_vec = self
            .pixels
            .data()
            .iter()
            .map(|x| x.norm())
            .collect::<Vec<f32>>();
        let pixels = TooDee::from_vec(self.width(), self.height(), pixels_vec);

        let (min, max) = pixels
            .data()
            .iter()
            .fold((f32::MAX, f32::MIN), |(a, b), x| (a.min(*x), b.max(*x)));
        let delta_inverse = 1.0 / (max - min);

        ImageBuffer::from_fn(self.width() as u32, self.height() as u32, |x, y| {
            let value = ((pixels[y as usize][x as usize] - min) * delta_inverse)
                .powf(texture_properties.gamma)
                * 2_f32.powf(texture_properties.exposure);
            Rgb([(match texture_properties.coherent_light {
                true => value.powi(2),
                false => value,
            } * 255.0) as u8; 3])
        })
    }

    /// Write the texture to the specified path.
    ///
    /// It should be mentioned that each pixel value is squared,
    /// because the energy of an electromagnetic wave is proportional to its amplitude $E \sim A^2$.
    /// Thus, an electromagnetic wave's intensity is proportional
    /// to its maximum electric field strength $I \sim E_0^2$.
    /// The sensor of a camera measures $I$.
    /// Because the Fraunhofer diffraction uses the wave's amplitude,
    /// it processes the square root of each pixel's value.
    /// To reverse this operation each pixel needs to be squared when writing.
    ///
    /// Note that the exposure is calculated by $v' = v \cdot 2^{exposure}$
    /// and the gamma is calculated by $v' = v^{gamma}$.
    pub fn write<P: AsRef<Path>>(&self, path: P) -> ImageResult<()> {
        self.to_image_buffer(TextureProperties::default())
            .save(path)
    }
}

impl Texture<Srgb> {
    pub fn to_image_buffer(
        &self,
        texture_properties: TextureProperties,
    ) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
        let (min, max) = self
            .pixels
            .data()
            .iter()
            .fold((f32::MAX, f32::MIN), |(a, b), x| {
                (
                    a.min(x.relative_luminance().luma),
                    b.max(x.relative_luminance().luma),
                )
            });

        ImageBuffer::from_fn(self.width() as u32, self.height() as u32, |x, y| {
            let rgb = self.pixels[y as usize][x as usize];
            let slice = [rgb.red, rgb.green, rgb.blue];

            Rgb(slice
                .map(|x| (x - min) / (max - min))
                .map(|x| x.powf(texture_properties.gamma) * 2f32.powf(texture_properties.exposure))
                .map(|x| match texture_properties.coherent_light {
                    true => x.powi(2),
                    false => x,
                })
                .map(|x| (x * 255.0) as u8))
        })
    }

    /// Write the texture to the specified path.
    ///
    /// It should be mentioned that each pixel value is squared,
    /// because the energy of an electromagnetic wave is proportional to its amplitude $E \sim A^2$.
    /// Thus, an electromagnetic wave's intensity is proportional
    /// to its maximum electric field strength $I \sim E_0^2$.
    /// The sensor of a camera measures $I$.
    /// Because the Fraunhofer diffraction uses the wave's amplitude,
    /// it processes the square root of each pixel's value.
    /// To reverse this operation each pixel needs to be squared when writing.
    ///
    /// Note that the exposure is calculated by $v' = v \cdot 2^{exposure}$
    /// and the gamma is calculated by $v' = v^{gamma}$.
    pub fn write<P: AsRef<Path>>(&self, path: P) -> ImageResult<()> {
        self.to_image_buffer(TextureProperties::default())
            .save(path)
    }
}

impl<T: Default> Index<usize> for Texture<T> {
    type Output = [T];

    fn index(&self, index: usize) -> &Self::Output {
        &self.pixels[index]
    }
}

impl<T: Default> IndexMut<usize> for Texture<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.pixels[index]
    }
}
